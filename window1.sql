WITH RankedCustomers AS (
  SELECT
    cust_id,
    channel_id,
    RANK() OVER (PARTITION BY channel_id ORDER BY SUM(amount_sold) DESC) AS sales_rank
  FROM
    sh.sales
  WHERE
    EXTRACT(YEAR FROM time_id) IN (1998, 1999, 2001)
  GROUP BY
    cust_id, channel_id
)

SELECT
  cust_id,
  channel_id,
  TO_CHAR(SUM(amount_sold), 'FM999999999.00') AS formatted_total_sales
FROM
  sh.sales
WHERE
  cust_id IN (
    SELECT
      cust_id
    FROM
      RankedCustomers
    WHERE
      sales_rank <= 300
      AND channel_id = 4
  )
GROUP BY
  cust_id, channel_id;

  
  SELECT
  c.cust_id,
  c.cust_first_name,
  s.channel_id
FROM
  sh.customers c
JOIN
  sh.sales s ON c.cust_id = s.cust_id
GROUP BY
  c.cust_id, c.cust_first_name, s.channel_id;
